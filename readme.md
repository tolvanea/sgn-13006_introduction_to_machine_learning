# SGN-13006_Introduction_to_machine_learning

This repo contains my work on course _SGN-13006 Introduction to machine learning_ (2018) at Tampere University of Technology.

Here I have rewritten matlab exercises in python. I share this repository because I hope that course staff will reimplement that course in python some day, and I hope these codes help in that process.


Only on week-2 I have exercise template available that does not contain my own solution (it is `cifar_10_read_data.py`). Other week exercises I have solved directly in one `.py` file. By the way, week-2 requires `cifar-10-batches-mat` dataset folder, and week-6 requires `gym` python repo folder. These should be placed in folders of corresponding weeks.

All python code in this repository can be used freely under "_Do what ever the fuck you want"_ -licence. Also MIT or CC0 are applicable licences. So the code is public domain and there is absolutely no restriction whatsoever.
