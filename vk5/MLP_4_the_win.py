"""
Problems 1,2,3 in exercise 3, SGN-13006 Introduction to machine learnign 
and pattern recognition. A.T.
"""
import scipy.io as sio
import numpy as np
import scipy.stats
import os
import time
import sys
#from multiprocessing import Pool
from matplotlib import pyplot as plt
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras import backend



batch_size = 32
num_classes = 10
epochs = 100
data_augmentation = True
num_predictions = 20
save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'keras_cifar10_trained_model.h5'


# The main function is in the bottom of this file, start reading there. Ok?


    
    
    
    


class Data():
    """ This class is for general data-reading related things."""
    
    train_files = ["data_batch_{}.mat".format(i+1) for i in range(5)]
    test_file = 'test_batch.mat'
    meta_file = 'batches.meta.mat'
    num_classes = 10

    def __init__(self, data_dir=None):
        """Information about test data. If your current directory has not directory
        'cifar-10-batches-mat' in it, change class variable 'current_dir' to some 
        preferred string."""
        
        # data_batch_1.mat, data_batch_2.mat...
        if data_dir is None:
            self.cifar10_dir = os.path.join(os.getcwd(), "cifar-10-batches-mat")
        else:
            self.cifar10_dir = data_dir
        
        M = sio.loadmat(os.path.join(self.cifar10_dir, self.meta_file))
        self.label_names = [str(name[0][0]) for name in M["label_names"]]
        #num_classes = len(self.label_names)

        
    def read(self, file, flatten=False, binary_labels=False):
        """Read image sets and corresponding labels."""
        
        print("    Reading: ", file[-16:])
        dict = sio.loadmat(file)
        
        # 'data' is marix of shape 10000*3072 (= 10000 images). 
        # Image is vector of 3072 (= 32*32*3) colors (= value 0-255). 
        data = dict["data"]        # data.shape = (10000, 3072)
        
        # 'labels' is row-vector with corresponding class number 0-9
        labels = dict["labels"]    # labels.shape = (10000, 1)
        
        if flatten:
            imgs = np.empty((len(labels), 32,32,3), dtype=np.float_)
            # by default, iteratror 'data' goes through every row
            for i, img in enumerate(data):
                imgs[i,:,:,:] = self.convert_flat_image_to_3d_matrix_image(img)
        else:
            imgs = labels
            
        if binary_labels:
            labs = self.convert_labels_to_binary_format(labels[:,0], self.num_classes)
        else:
            labs = labels[:,0]
        
        return imgs, labs

    def read_and_concatenate(self, files, flatten=False, binary_labels=False):
        """Combine multiple (training) files into one matrix/vector."""
        
        l = len(files)
        
        for i, file in enumerate(files):
            tr_data, tr_labels = self.read(os.path.join(self.cifar10_dir, file),
                                           flatten=flatten, binary_labels=binary_labels)
            if i == 0: # create out-put files based on read matrix dimensions
                N = tr_data.shape[0] 
                pixels = tr_data.shape[1:]
                labdims = tr_labels.shape[1:]
                
                all_data = np.empty((l*N, *pixels), dtype=tr_data.dtype)
                all_labels = np.empty((l*N, *labdims), dtype=tr_labels.dtype)
            
            s = i*N      # start idx
            e = (i+1)*N  # end idx
            all_data[s:e,...] = tr_data
            all_labels[s:e, ...] = tr_labels
            
        return all_data, all_labels
    
    def convert_flat_image_to_3d_matrix_image(self, x, nx=32, ny=32):
        """ Converts flattened image format to 3d-matrix. In other words this reads 
        cifar image data rows and makes them more nice datastructure. """
        assert len(x.shape) == 1
        assert x.shape[0] == nx*ny*3
        
        img = np.empty((nx,ny,3), dtype=x.dtype)
        
        l = nx*ny
        
        img[:,:,0] = np.reshape(x[:l], (nx, ny))    # red
        img[:,:,1] = np.reshape(x[l:2*l], (nx, ny)) # green
        img[:,:,2] = np.reshape(x[2*l:], (nx, ny))  # blue
        
        return img
    
    def convert_labels_to_binary_format(self, labels, num_classes):
        """ Converts labels in form [1,2,0,1] to form [[0,1,0], [0,0,1], 
        [1,0,0], [0,1,0]].  Used in MLP-trainging and comparison"""
        
        assert len(labels.shape) == 1
        assert np.issubdtype(labels.dtype, np.integer)
        
        bin_labels = np.zeros((len(labels), num_classes), dtype=labels.dtype)
        for i in range(len(labels)):
            bin_labels[labels[i]] = 1
            
        return bin_labels

class Utils:

    def do_for_all_images(func, iterable, args=()):
        """ Apply given function to each image in array. Return matrix of results."""
        for i, val in enumerate(iterable):
            output = func(val, *args)
            #assert isinstance(output, np.ndarray)  # given function must return ndarray
            if i == 0:
                dim0 = len(iterable)
                if np.isscalar(output):
                    rest_dims = ()
                else:
                    rest_dims = output.shape
                ret = np.empty((dim0,*rest_dims), dtype=iterable.dtype)
            ret[i,...] = output
        return ret
        
        
        return sum/len(gt)

    def labels_evaluate(lab1, lab2):
        """ Compare accuracy of  predicted labels to known lables. Used in MLP. Labels 
        can be in form  [1,2,0,1] or in form [[0,1,0], [0,0,1], [1,0,0], [0,1,0]]."""
        assert lab1.shape == lab2.shape
        assert len(lab1.shape) <= 2
        
        return np.sum(lab1 == lab2) / len(lab1)



def attempt_1():
    data = Data("../vk2/cifar-10-batches-mat") # change this path
    
    print("backend.image_data_format()", backend.image_data_format())
    
    """Problems 1,2,3."""
    tr_data, tr_labels = data.read_and_concatenate(Data.train_files[0:1], binary_labels=True)
    te_imgs, te_labels = data.read_and_concatenate([Data.test_file], binary_labels=True)
    
    if backend.image_data_format() != "channels_last":
        raise Exception("Modify code, put color channels first")
    tr_data = tr_data[:200,...] # images are now side ways, but who cares
    te_imgs = te_imgs[:100,...]
    tr_labels = tr_labels[:200,...]
    te_labels = te_labels[:100,...]
    
    
    classifier = Sequential()
    classifier.add(
        Conv2D( filters=32,         # number of output filters in the convolution
                kernel_size=(3, 3), #  height and width of the 2D convolution window
                input_shape = (32, 32, 3), 
                activation = 'relu'))    
    classifier.add(MaxPooling2D(pool_size = (2, 2)))
    classifier.add(Flatten())
    classifier.add(Dense(units = 128, activation = 'relu'))
    classifier.add(Dense(units = 10, activation = 'sigmoid'))
    
    classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
    
#    train_datagen = ImageDataGenerator( rescale = 1./255,
#                                        shear_range = 0.2,
#                                        zoom_range = 0.2,
#                                        horizontal_flip = True)
#                                        test_datagen = ImageDataGenerator(rescale = 1./255)
#                                        training_set = train_datagen.flow_from_directory(
#                                                'training_set',
#                                                  target_size = (64, 64),
#                                                batch_size = 32,
#                                                class_mode = 'binary')
#                                        test_set = test_datagen.flow_from_directory(
#                                                'test_set',
#                                                target_size = (64, 64),
#                                                batch_size = 32,
#                                                class_mode = 'binary')
    classifier.fit(x=tr_data, y=tr_labels, epochs=50, batch_size=100)
    
    predictions = classifier.predict(te_imgs)
    predictions = predictions[:,0]
    print("predictions.shape, te_labels.shape", predictions.shape, te_labels.shape)
    
    print("Accuracy", cifar_10_evaluate(predictions, te_labels))
    
    
def attempt_2():
    from keras.datasets import cifar10
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    print('x_train shape:', x_train.shape)
    print('y_train shape:', y_train.shape)


def main():
    attempt_2()
    
    
    
    


    
    
if __name__ == "__main__":
    main()
