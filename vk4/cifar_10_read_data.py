"""
Rewrite of 'cifar_10_read_data.m' in python. Exercise 2 in SGN-13006 Introduction 
to machine learnign and pattern recognition

A.T.
"""


import scipy.io as sio 
import numpy as np
import os
from matplotlib import pyplot as plt
from matplotlib import animation

class conf:
    """Information about test data. If your current directory has not directory
    'cifar-10-batches-mat' in it, change class variable 'current_dir'."""
    
    # data_batch_1.mat, data_batch_2.mat...
    train_files = ["data_batch_{}.mat".format(i+1) for i in range(5)]
    test_file = 'test_batch.mat'
    # current_dir = "/home/username/your/path"  # or change path to this 
    current_dir = os.getcwd()  # 'getcwd()' equivalent to bash 'pwd'
    meta_file = 'batches.meta.mat'
    cifar10_dir = os.path.join(current_dir, "cifar-10-batches-mat")
    
    
    M = sio.loadmat(os.path.join(cifar10_dir, meta_file))
    label_names = [str(name[0][0]) for name in M["label_names"]]
    #print(label_names)
    
def main():
    """Print random pictures from 5 training sets and one test set."""
    
    # print_content_of_meta_file() 
    
    print("'Showing training data...")
    for filename in conf.train_files:
        tr_data, tr_labels = read_data(os.path.join(conf.cifar10_dir, filename))
        plot_data(tr_data, tr_labels, filename)
        
    print("'Showing test data...")
    te_data, te_labels = read_data(os.path.join(conf.cifar10_dir, conf.test_file))
    plot_data(te_data, te_labels, conf.test_file)
    
def read_data(file):
    """Read image sets and corresponding labels."""
    
    print("    Reading: ", file[-16:])
    dict = sio.loadmat(file)
    
    # 'data' is marix of shape 10000*3072 (= 10000 images). 
    # Image is vector of 3072 (= 32*32*3) colors (= value 0-255). 
    data = dict["data"]        # data.shape = (10000, 3072)
    # 'labels' is row-vector with corresponding class number 0-9
    labels = dict["labels"]    # labels.shape = (10000, 1)
    
    #print("data[:3,:3], labels[:3]", data[:3,:3], labels[:3])
    #print("data[:3,:3].dtype, labels[:3].dtype", data[:3,:3].dtype, labels[:3].dtype)
    #print("type(data[:3,:3]), type(labels[:3])", type(data[:3,:3]), type(labels[:3]))
    
    return data, labels[:,0]
                

def plot_data(data, labels, title=""):
    """Plot random pictures as animation. Figures: 32x32=1024 pixels r,g,b 
    channels."""
    nx=32
    ny=32
    fig, ax = plt.subplots(1, 1, figsize=(5,4))
    
    def init():
        im.set_data(np.zeros((nx, ny)))
    
    def animate(frame_idx):
        idx = frame_idx * 2000
        if idx >= 10000:
            plt.close(fig)
            return None
        
        data_sample = data[idx]
        img_r = data_sample[:1024]
        img_g = data_sample[1024:2048]
        img_b = data_sample[2048:]
        
        data_img = np.zeros((nx, ny, 3))
        data_img[:,:,0] = np.reshape(img_r, (nx, ny))
        data_img[:,:,1] = np.reshape(img_g, (nx, ny))
        data_img[:,:,2] = np.reshape(img_b, (nx, ny))
        
        im = ax.imshow(data_img/255, cmap='gist_gray_r', vmin=0, vmax=1)
        ax.set_title(conf.label_names[labels[idx]]);
        fig.suptitle(title)
            
        return im
    
    anim = animation.FuncAnimation(fig, animate, frames=nx * ny,
                                interval=500)
    
    plt.show()
    
def print_content_of_meta_file():
    """Test function for finding out what the hell does these mat-files contain.
    """
    M = sio.loadmat(os.path.join(conf.cifar10_dir, conf.meta_file))
    print("Contents of meta_file '{}'".format(conf.meta_file))
    for key in M:
        print("    Key:", key) # , " : ", M[key]
        if key == "label_names":
            for i, name in enumerate(M["label_names"]):
                print("    ",i , str(name[0][0]))

if __name__ == "__main__":
    main()
