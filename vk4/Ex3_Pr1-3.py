"""
Problems 1,2,3 in exercise 3, SGN-13006 Introduction to machine learnign 
and pattern recognition. A.T.
"""

import scipy.io as sio 
import numpy as np
import scipy.stats
import os
import sys
import time
#from multiprocessing import Pool
from matplotlib import pyplot as plt






# The main function is in the bottom of this file, start reading there. Ok?


    
    
    
    


class Data():
    """ This class is for general data-reading related things."""
    
    train_files = ["data_batch_{}.mat".format(i+1) for i in range(5)]
    test_file = 'test_batch.mat'
    meta_file = 'batches.meta.mat'
    num_classes = 10

    def __init__(self, data_dir=None):
        """Information about test data. If your current directory has not directory
        'cifar-10-batches-mat' in it, change class variable 'current_dir' to some 
        preferred string."""
        
        # data_batch_1.mat, data_batch_2.mat...
        if data_dir is None:
            self.cifar10_dir = os.path.join(os.getcwd(), "cifar-10-batches-mat")
        else:
            self.cifar10_dir = data_dir
        
        M = sio.loadmat(os.path.join(self.cifar10_dir, self.meta_file))
        self.label_names = [str(name[0][0]) for name in M["label_names"]]
        #num_classes = len(self.label_names)

        
    def read(self, file):
        """Read image sets and corresponding labels."""
        
        print("    Reading: ", file[-16:])
        dict = sio.loadmat(file)
        
        # 'data' is marix of shape 10000*3072 (= 10000 images). 
        # Image is vector of 3072 (= 32*32*3) colors (= value 0-255). 
        data = dict["data"]        # data.shape = (10000, 3072)
        
        # 'labels' is row-vector with corresponding class number 0-9
        labels = dict["labels"]    # labels.shape = (10000, 1)
        
        imgs = np.empty((len(labels), 32,32,3), dtype=np.float_)
        # by default, iteratror 'data' goes through every row
        for i, img in enumerate(data):
            imgs[i,:,:,:] = self.convert_flat_image_to_3d_matrix_image(img)
        
        return imgs, labels[:,0]

    def read_and_concatenate(self, files):
        """Combine multiple (training) files into one matrix/vector."""
        
        l = len(files)
        
        for i, file in enumerate(files):
            tr_data, tr_labels = self.read(os.path.join(self.cifar10_dir, file))
            if i == 0: # create out-put files based on read matrix dimensions
                N = tr_data.shape[0] 
                pixels = tr_data.shape[1:]
                
                all_data = np.empty((l*N, *pixels), dtype=tr_data.dtype)
                all_labels = np.empty(l*N, dtype=tr_labels.dtype)
            
            s = i*N      # start idx
            e = (i+1)*N  # end idx
            all_data[s:e,:,:,:] = tr_data
            all_labels[s:e] = tr_labels
            
        return all_data, all_labels
    
    def convert_flat_image_to_3d_matrix_image(self, x, nx=32, ny=32):
        """ Converts flattened image format to 3d-matrix. In other words this reads 
        cifar image data rows and makes them more nice datastructure. """
        assert len(x.shape) == 1
        assert x.shape[0] == nx*ny*3
        
        img = np.empty((nx,ny,3), dtype=x.dtype)
        
        l = nx*ny
        
        img[:,:,0] = np.reshape(x[:l], (nx, ny))    # red
        img[:,:,1] = np.reshape(x[l:2*l], (nx, ny)) # green
        img[:,:,2] = np.reshape(x[2*l:], (nx, ny))  # blue
        
        return img
    

class Prob1:
    """ This class contains function related to problem 1. """
    
    def cifar_10_features(img):
        """ Valulate mean color-values Red, Green, Blue for picture x. 
        
        Args:
            x:  ndarray of size x*y*3. Colors are in range 0-255"""
            
        assert len(img.shape) == 3
        assert img.shape[2] == 3
        
        return img.mean(axis=(0,1))


    def cifar_10_bayes_learn(F,labels):
        """ Calculates means, variances and covariances for training-data features, 
        which is mean of pixel-values.
        
        Args:
            F:      array of size N*3, containing mean values for each picture.
            labels: corresponding labels
            """
        
        assert F.shape[0] == labels.shape[0]    # same sizes
        assert F.shape[1] == 3                  # three colors
        
        num_of_classes = Data.num_classes
        
        mu = np.empty((num_of_classes,3), dtype=np.float_)
        sigma = np.empty((num_of_classes,3), dtype=np.float_)
        P = np.empty((num_of_classes), dtype=np.float_)
        
        
        for cl in range(num_of_classes):
            occurence = np.count_nonzero(labels == cl)

            mu[cl,:] = np.mean(F[labels == cl], axis=0)
            sigma[cl,:] = np.std(F[labels == cl], axis=0)
            P[cl] = occurence / len(labels)
            
        return mu, sigma, P
        
        
    def cifar_10_bayes_classify(f, mu, sigma, p):
        """ Estimate class using gaussian deviations of mean value of pixel. 
        Also assume that color channels are not correlated."""
        
        assert len(f.shape) == 1
        assert f.shape[0] == 3
            
        P_max = -1
        class_P_max = -1
        for cl in range(Data.num_classes):

            P = scipy.stats.norm.pdf(f, mu[cl], sigma[cl]).prod() * p[cl]
                                                         # ^ notice! this takes 
                                                         # product of 
                                                         # P(red)*P(green)*P(blue)
            
            if P > P_max:
                P_max = P
                class_P_max = cl
                
        
        return class_P_max
    
    
    def problem1(te_data, te_labels, tr_data, tr_labels):
        print("Problem 1")
        
        mean_colors = do_for_all_images(Prob1.cifar_10_features, tr_data)
        mu,sigma,p = Prob1.cifar_10_bayes_learn(mean_colors, tr_labels)
        
        te_mean_colors = do_for_all_images(Prob1.cifar_10_features, te_data)
        
        classes = do_for_all_images(Prob1.cifar_10_bayes_classify, te_mean_colors, 
                                    args=(mu, sigma, p))
        
        succes_rate = cifar_10_evaluate(classes, te_labels)
        print("succes_rate", succes_rate, "\n")
    
    
class Prob2:
    
    def cifar_10_bayes_learn_use_covariance(F,labels):
        """ Calculates means, variances and covariances for training-data features, 
        which is mean of pixel-values.
        
        Args:
            F:      array of size N*3, containing mean values for each picture.
            labels: corresponding labels
            """
        
        assert F.shape[0] == labels.shape[0]    # same sizes
        assert F.shape[1] % 3 == 0              # three colors
        
        num_of_classes = Data.num_classes
        num_of_features = F.shape[1]
        
        mu = np.empty((num_of_classes, num_of_features), 
                      dtype=np.float_)
        cov = np.empty((num_of_classes, num_of_features, num_of_features), 
                       dtype=np.float_)
        P = np.empty((num_of_classes), dtype=np.float_)
        
        
        for cl in range(num_of_classes):
            occurence = np.count_nonzero(labels == cl)

            mu[cl,:] = np.mean(F[labels == cl], axis=0)
            cov[cl,:,:] = np.cov(F[labels == cl].T)
            P[cl] = occurence / len(labels)
            
        return mu, cov, P
    
    
    def cifar_10_bayes_classify_use_covariance(f, mu, cov, p):
        """ Estimate class using gaussian deviations of mean value of pixel. 
        Take correlation of color channels into account.
        
        Args:
            f:      vector "features", i.e. three colors, or three colors and subimages
            mu      means of features for classes of training data 
            cov     covariances of features for classes of training data
            p       probabilities of classes in training data
        """
        
        assert len(f.shape) == 1    # 1d vector
        assert f.shape[0] % 3 == 0  # three colors
            
        P_max = -1
        class_P_max = -1
        for cl in range(Data.num_classes):
            
            try:
                P = scipy.stats.multivariate_normal.pdf(f, mu[cl], cov[cl]) * p[cl]
            except np.linalg.LinAlgError:
                P = scipy.stats.multivariate_normal.pdf(f, mu[cl], cov[cl], allow_singular=True) * p[cl]
            
            if P > P_max:
                P_max = P
                class_P_max = cl
                
        
        return class_P_max
    
    
    def problem2(te_data, te_labels, tr_data, tr_labels):
        print("Problem 2")
        mean_colors = do_for_all_images(Prob1.cifar_10_features, tr_data)
        mu,cov,p = Prob2.cifar_10_bayes_learn_use_covariance(mean_colors, tr_labels)
        
        te_mean_colors = do_for_all_images(Prob1.cifar_10_features, te_data)

        classes = do_for_all_images(Prob2.cifar_10_bayes_classify_use_covariance, 
                                    te_mean_colors, 
                                    args=(mu, cov, p))
        
        succes_rate = cifar_10_evaluate(classes, te_labels)
        print("covariance of colors, succes_rate", succes_rate, "\n")
        

class Prob3:
    def cifar_10_features_subimage(img,n=1) :
        """ Valulate mean color-values Red, Green, Blue for picture x. 
        
        Args:
            x:  ndarray of size x*y*3. Colors are in range 0-255
            n:  number of subpictures to divide one side,  n=2 --> 4 subpictures
        """
            
        assert len(img.shape) == 3           # image matrix: x*y*3
        assert img.shape[2] == 3             # three colors
        assert img.shape[0] == img.shape[1]  # square images only
        assert img.shape[0]%n == 0           # must be divisable
        
        l = img.shape[0]
        a = img.shape[0] // n
        
        f = np.empty((n * n * 3), dtype=np.float_)
        
        for i in range(n):
            for j in range(n):
                f0 = 3*n*i + 3*j
                f1 = 3*n*i + 3*j + 3
                i0 = i*a
                i1 = i*a+a
                f[f0:f1] = img[i0:i1, i0:i1, :].mean(axis=(0,1))
        
        return f
    
    
    def problem3(te_data, te_labels, tr_data, tr_labels):
        print("Problem 3")
        
        def calc_with_window_size(div):
            mean_colors = do_for_all_images(Prob3.cifar_10_features_subimage, 
                                            tr_data, args=(div,))
            mu,cov,p = Prob2.cifar_10_bayes_learn_use_covariance(mean_colors, tr_labels)
            
            te_mean_colors = do_for_all_images(Prob3.cifar_10_features_subimage, 
                                               te_data, args=(div,))
            classes = do_for_all_images(Prob2.cifar_10_bayes_classify_use_covariance, 
                                        te_mean_colors, 
                                        args=(mu, cov, p))
            
            succes_rate = cifar_10_evaluate(classes, te_labels)
            
            
            return (succes_rate, div)
        
        x = []
        y = []
        
        # pool = Pool(processes=4)                            # multithreading
        #res = pool.map(calc_with_window_size, [1, 2, 4, 8])  # shit, it won't work
        res = map(calc_with_window_size, [1, 2, 4, 8])

        
        for succes_rate, div in res:
            print("covariance of colors and subimages (div={}), succes_rate:".format(div), 
                  succes_rate, "\n")
            x.append(div)
            y.append(succes_rate)
        
        plt.semilogx(x,y)
        plt.xlabel("window edge divisions")
        plt.ylabel("succes rate")
        plt.show()


def do_for_all_images(func, iterable, args=()):
    """ Apply given function to each image in array. Return matrix of results."""
    for i, val in enumerate(iterable):
        output = func(val, *args)
        #assert isinstance(output, np.ndarray)  # given function must return ndarray
        if i == 0:
            dim0 = len(iterable)
            if np.isscalar(output):
                rest_dims = ()
            else:
                rest_dims = output.shape
            ret = np.empty((dim0,*rest_dims), dtype=iterable.dtype)
        ret[i,...] = output
    return ret
    
    
def cifar_10_evaluate(pred, gt):
    """ Compare accuracy of some predicted labels to known lables. 
    Here 'gt' means "ground truth"  which is same as true labels."""
    assert len(pred) == len(gt)
    assert len(pred.shape) == 1  # is vector
    
    sum = 0
    for i in range(len(gt)):
        if pred[i] == gt[i]:
            sum += 1
    
    return sum/len(gt)




def main():
    data = Data("../vk2/cifar-10-batches-mat") # change this path
    
    """Problems 1,2,3."""
    te_imgs, te_labels = data.read_and_concatenate([Data.test_file])
    tr_data, tr_labels = data.read_and_concatenate(Data.train_files[0:2])
    
    Prob1.problem1(te_imgs, te_labels, tr_data, tr_labels)
    
    Prob2.problem2(te_imgs, te_labels, tr_data, tr_labels)
    
    Prob3.problem3(te_imgs, te_labels, tr_data, tr_labels)
    
    
    
    


    
    
if __name__ == "__main__":
    main()
