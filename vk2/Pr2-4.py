"""
Problems 2,3,4 in exercise 2, SGN-13006 Introduction to machine learnign 
and pattern recognition. A.T.
"""

import scipy.io as sio 
import numpy as np
import os
import sys
from cifar_10_read_data import read_data, conf
import time


def main():
    """Problems 2,3,4. For problem 1, run cifar_10_read_data.py"""
    
    # read data and labels from training batch 1
    data, labels = read_data(os.path.join(conf.cifar10_dir, conf.train_files[0]))
    
    print("\nPr. 2")
    pred_P_true = cifar_10_evaluate(labels, labels) # "ground trurth" means label
    print("Ground truth:", pred_P_true , "~1")
    
    print("\nPr. 3")
    pred_random_labels = cifar_10_rand(data)
    pred_P_rnd = cifar_10_evaluate(pred_random_labels, labels)
    print("Random evaluate:", pred_P_rnd, "~0.1")
    
    print("\nPr.4")
    problem_4(conf)
    
def cifar_10_evaluate(pred, gt):
    """ Problem 2: Compare accuracy of some predicted labels to known lables. 
    'gt' means "ground truth"  which is same as true labels."""
    assert len(pred) == len(gt)
    assert len(pred.shape) == 1  # is vector
    
    sum = 0
    for i in range(len(gt)):
        if pred[i] == gt[i]:
            sum += 1
    
    return sum/len(gt)

def cifar_10_rand(x):
    """ Problem 3: generate random predictions to labels."""
    assert len(x.shape) == 2
    assert x.shape[1] == 3072
    
    l = x.shape[0]
    return np.random.randint(0, 10, l)

def problem_4(conf):
    """Calulculate and print nearest neighbour. Because it takes forever, it is 
    divided in parts. Results are printed in progress."""
    te_data, te_labels = read_data(os.path.join(conf.cifar10_dir, conf.test_file))
    # use all five training batches. Change to 1 to speed up
    test_samples = 5
    if test_samples > 1:
        print("\nThis will take a long time")
        
    tr_files = conf.train_files[0:test_samples]
    tr_data, tr_labels = read_and_concatenate_train_datas_in_one_chunk(
                                                conf.cifar10_dir, tr_files)
    
    parts = 100  # number of parts
    N = len(te_labels) // parts  # inverval of test-samples in part print
    prediction_parts = np.zeros(parts, dtype=np.float_)
    print("\nNearest Neighbour, part {}, training batches:{}"
                            .format(parts, list(range(1,test_samples+1))))
    print("   |---------------------------------------|\n     ", end="")
    for i in range(parts):
        pred_NN_labels = cifar_10_1NN(te_data[i*N:(i+1)*N, :], tr_data, tr_labels)
        pred_P_NN = cifar_10_evaluate(pred_NN_labels, te_labels[i*N:(i+1)*N])
        prediction_parts[i] = pred_P_NN
        print("({}/{}): {:.4f},  mean: {:.4f}"
              .format(i+1, parts, pred_P_NN, prediction_parts[:i+1].mean()))
        
        
def cifar_10_1NN(x,trdata,trlabels):
    """ Problem 4: Nearest neighbour classification. Compares pictures 
    pixel-wise."""
    assert len(x.shape) == len(trdata.shape) == 2
    assert len(trlabels.shape) == 1
    assert x.shape[1] == trdata.shape[1] == 3072
    assert trdata.shape[0] == trlabels.shape[0]
    
    output_labels = np.empty(len(x), dtype=trlabels.dtype)
    
    #print("## Progress for nearest neighbour classifier: ##")
    print("    ", end="")
    parts = 40
    intrv = x.shape[0]/parts  # interval of #-printing
    x.shape[0]
    start_time = time.time()
    for i in range(x.shape[0]):
        # print progress wtih '#' marks with print-size 10*100
        if (i % intrv > (i+1) % intrv):
            print(".",end="")
            sys.stdout.flush()
        
        test_img = x[i, :]  # 3072-pixel vector
        dist = np.sum((trdata - test_img)**2, axis=1)  # pixel-wise comparison
        min_idx = np.argmin(dist)
        output_labels[i] = trlabels[min_idx]
        
    end_time = time.time()
    interval = end_time - start_time
    print(" {:4} s, ".format(int(interval)), end="")
        
    return output_labels

def read_and_concatenate_train_datas_in_one_chunk(basepath, testfiles):
    """Combine multiple training-files into one matrix/vector."""
    tr_data0, tr_labels0 = read_data(os.path.join(conf.cifar10_dir, testfiles[0]))
    l = len(testfiles)
    N = tr_data0.shape[0]  # number of images in set is supposed to be constant(=10000)
    pixels = tr_data0.shape[1]
    all_data = np.empty((l*N, pixels), dtype=tr_data0.dtype)
    all_labels = np.empty(l*N, dtype=tr_labels0.dtype)
    
    all_data[:N,:] = tr_data0
    all_labels[:N] = tr_labels0
    
    for i, file in enumerate(testfiles):
        if i == 0:
            continue  # First one already added
        
        tr_data, tr_labels = read_data(os.path.join(conf.cifar10_dir, file))
        s = i*N      # start idx
        e = (i+1)*N  # end idx
        all_data[s:e,:] = tr_data
        all_labels[s:e] = tr_labels
        
    return all_data, all_labels
        
    
    
if __name__ == "__main__":
    main()
