"""
Cource: Introduction to machine learnign and pattern recognition
Exercise 6, reinforced learning, Q-learning

A.T
"""

import gym  # you need 'gym' folder in same directory
import random
import numpy as np
import time

# Training
def train(env, next_state, next_reward):
    RESETS = 700
    RND_STEPS = 500
    gamma = 0.8
    states_filled = 0.0
    
    for i in range(RESETS):
        observation = env.reset();
        past_observation = observation
        for j in range(RND_STEPS):
            
            action = np.random.randint(6)
            observation, reward, done, info = env.step(action)
            
            next_state[past_observation, action] = observation
            next_reward[past_observation, action] = reward + gamma * np.max(next_reward[observation])
            
            past_observation = observation
            
            if done:
                break
            elif j == RND_STEPS - 1:
                print("Damn")
                
        if i % (RESETS//10) == 0:
            # Determines how many of states are chekced out of all possibilities
            states_filled = 1 - np.sum((next_state + 1000) == 0) / np.prod(next_state.shape)
            print("training {}, States filled: {:.3f}".format(i/RESETS, states_filled))
            
    

# Testing
def test(env, next_reward):
    test_tot_reward_TOT = 0
    test_tot_actions_TOT = 0
    for i in range(10):
        test_tot_reward = 0
        test_tot_actions = 0
        past_observation = -1
        observation = env.reset();
        for t in range(50):
            test_tot_actions = test_tot_actions+1
            action = np.argmax(next_reward[observation])
            if (observation == past_observation):
                # This is done only if gets stuck
                action = np.random.randint(6)
            past_observation = observation
            observation, reward, done, info = env.step(action)
            test_tot_reward = test_tot_reward + reward
            env.render()
            time.sleep(0.05)
            if done:
                break
        print("---- run {} ---".format(i))
        print("Total reward: {}".format(test_tot_reward))
        print("Total actions: {}".format(test_tot_actions))
        test_tot_reward_TOT += test_tot_reward
        test_tot_actions_TOT += test_tot_actions
    
    print("\n--- Average out of 10 ---")
    print("Total average reward: {}".format(test_tot_reward_TOT/10))
    print("Total average actions: {}".format(test_tot_actions_TOT/10))


def main():
    env = gym.make("Taxi-v2")
    next_state = -1000*np.ones((501,6), dtype=np.int)
    next_reward = -1000*np.ones((501,6))
    
    train(env, next_state, next_reward)
    test(env, next_reward)
    
main()


"""
The Taxi Problem
    from "Hierarchical Reinforcement Learning with the MAXQ Value Function Decomposition" by Tom Dietterich
    
Description:
    There are four designated locations in the grid world indicated by R(ed), B(lue), G(reen), and Y(ellow). When the episode starts, the taxi starts off at a random square and the passenger is at a random location. The taxi drive to the passenger's location, pick up the passenger, drive to the passenger's destination (another one of the four specified locations), and then drop off the passenger. Once the passenger is dropped off, the episode ends.
    
Observations: 
    There are 500 discrete actions since there are 25 taxi positions, 5 possible locations of the passenger (including the case when the passenger is the taxi), and 4 destination locations. 
    
Actions: 
    There are 6 discrete deterministic actions:
    - 0: move south
    - 1: move north
    - 2: move east 
    - 3: move west 
    - 4: pickup passenger
    - 5: dropoff passenger
    
Rewards: 
    There is a reward of -1 for each action and an additional reward of +20 for delievering the passenger. There is a reward of -10 for executing actions "pickup" and "dropoff" illegally.
    
    Rendering:
    - blue: passenger
    - magenta: destination
    - yellow: empty taxi
    - green: full taxi
    - other letters: locations
"""
