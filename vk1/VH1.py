import numpy as np
import matplotlib.pyplot as plt

 
def matrixmethod(xdata,ydata):
    """General polynomial least squares fit with matrix method."""
    x = np.asarray(xdata)
    y = np.asarray(ydata)
    assert len(x) == len(y)
    assert len(x.shape) == 1
    assert len(y.shape) == 1
    
    N = len(x)
    
    X = np.empty((len(x), 2))
    X[:, 0] = x
    X[:, 1] = np.ones(N)
    
    beta = np.linalg.inv(X.T @ X) @ X.T @ y
    #beta = np.linalg.lstsq(X, y)[0]
    
    a = beta[0]
    b = beta[1]
    
    assert np.isscalar(a) and np.isscalar(np.isscalar(b))
    
    return a, b


def linfit(xdata,ydata):
    """ My overcomplicated derivation"""
    x = np.asarray(xdata)
    y = np.asarray(ydata)
    assert len(x) == len(y)
    assert len(x.shape) == 1 and len(y.shape)  == 1
    
    N = len(x)
    
    a_nom = - 1/N * (y.sum())*(x.sum()) + (x*y).sum()
    a_denom = - 1/N * (x).sum()**2  + (x**2).sum()
    a = a_nom/a_denom
    
    
    b = 1/N * (y.sum() - a * x.sum())
    
    assert np.isscalar(a) and np.isscalar(np.isscalar(b))
    
    print(a, b)
    
    return a, b


def test_linfit(a=1, b=0, N=10):
    N = 10
    x = np.linspace(-1,1,N)
    y = a*x + b + np.random.random(len(x))*0.2
    
    a0, b0 = matrixmethod(x, y)
    a1, b1 = linfit(x, y)
    
    plt.scatter(x, y)
    plt.plot(x, a0*x + b0, label="matrix method")
    plt.plot(x, a1*x + b1, label="my linfit")
    
    plt.legend()
    
    plt.show()
    
    
def main():
    #test_linfit(1, 0, 10)
    test_linfit(-4, 1, 20)
    
    
main()
